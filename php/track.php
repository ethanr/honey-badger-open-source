<?php

$del = $_GET['delete'];
if (isset($del)){
    if ($del == 'db') {
        exec('rm -f targets.db');
        echo 'database deleted';
    } elseif ($del == 'log') {
        exec('rm -f log');
        echo 'log deleted';      
    }
    exit();
}

// initialize database
try {
    $dbname = 'targets.db';
    $db = new PDO('sqlite:'.$dbname);
} catch (Exception $error) {
    die($error);
}
// check for full load or ajax call
// if ajax, process and kill
$id = $_GET['id'];
if (isset($id)){
    $row = $db->query('SELECT * FROM targets WHERE id = '.$id)->fetch(PDO::FETCH_ASSOC); // return by column name only
    echo json_encode($row);
    $db = NULL;
    exit();
}
?>
<!DOCTYPE HTML>
<html>
<head>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#target').change(function() {
        $.get(location.href, 'id='+$('#target').val(), function(data){
            var json = $.parseJSON(data);
            var content = '<div align="left" class="iwcontent"> \
                           Source: ' + json.origin + ' @ ' + json.ip + ':' + json.port + '<br /> \
                           User-Agent: ' + json.agent + '<br /> \
                           Username: ' + json.user + '<br /> \
                           Hostname: ' + json.host + '<br /> \
                           Latitude: ' + json.lat + '<br /> \
                           Longitude: ' + json.lng + '<br /> \
                           Accuracy: ' + json.acc + '<br /> \
                           </div>';
            showMap(json.lat, json.lng, content);
        });
        //return false;
    });
    $('#target').change();
    //return false;
});
function showMap(lat, lng, content) {
    var latitude = lat;
    var longitude = lng;
    var coords = new google.maps.LatLng(latitude, longitude);
    var mapOptions = {
        zoom: 15,
        center: coords,
        mapTypeControl: true,
        navigationControlOptions: { style: google.maps.NavigationControlStyle.SMALL },
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    var marker = new google.maps.Marker({
        position: coords,
        map: map,
        title: "Your current location!"
    });
    var infowindow = new google.maps.InfoWindow({
        content: content
    });
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map, marker);
    });
    infowindow.open(map, marker);
}
</script>
<style type="text/css">
body {
    font-family: Arial, Helvetica, sans-serif;
    font-size: .75em;
}
.iwcontent {
    width: 300px;
}
#wrapper {
}
#targets {
    padding: 10px;
}
#mapContainer {
    padding: 10px;
}
#map {
    height: 600px;
    width: 800px;
    border:10px solid #eaeaea;
}
</style>
</head>
<body>
    <div id="wrapper" align="center">
        <div id="targets">
            Select a target:<br />
            <select id="target">
                <?php
                // populate options with targets from database
                foreach($db->query('SELECT id, time, user, ip, port FROM targets ORDER BY time') as $row) {
                    $reply =  '<option value="'.$row['id'].'">['.$row['time'].'] ';
                    if (strlen($row['user']) > 0) {
                        $reply .= $row['user'].'</option>';
                    } else {
                        $reply .= $row['ip'].':'.$row['port'].'</option>';
                    }
                    echo $reply;
                }
                $db = NULL;
                ?>
            </select>
        </div>
        <div id="mapContainer">
            <div id="map"></div>
        </div>
        <div><a href="log">view log</a> | <a href="track.php?delete=log">purge log</a> | <a href="track.php?delete=db">purge database</a></div>
    </div>
</body>
</html>
