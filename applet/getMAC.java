import java.util.Enumeration;
import java.net.*;

public class getMAC{
 
    public static void main(String[] args){
 
        try{

            String hostName = InetAddress.getLocalHost().getHostName();
            System.out.println(hostName);

            Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces();
            while ( e.hasMoreElements() ) {
                NetworkInterface networkInterface = (NetworkInterface) e.nextElement();
                System.out.println(networkInterface.getName().toString());
                byte[] mac = networkInterface.getHardwareAddress();
                if (mac != null) {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < mac.length; i++) {
                        sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));		
                    }
                    System.out.println(sb.toString());
                } else {
                    System.out.println("None");
                }
            }

	} catch (UnknownHostException e) {
 
            e.printStackTrace();
 
	} catch (SocketException e){
 
            e.printStackTrace();

        }

    }

}

